#!/bin/bash

set -e

inis=$(find S[0-9]* ER[0-9]* O[0-9]* -name *.ini 2>/dev/null | sort)

for ini in ${inis}; do
    echo -n "Validating $ini... "
    python tools/clf-to-omicron.py $ini -o test-omicron.ini 1> test-omicron.out
    python tools/clf-to-pyomega.py $ini -o test-pyomega.ini 1> test-pyomega.out
    python -c \
        "from gwdetchar.omega import config; " \
        "cp = config.OmegaConfigParser(); " \
        "cp.read(['test-pyomega.ini']);" \
        "cp.get_channel_blocks()"
    echo "OK"
done
